import { db } from '../database';
import { deleteSourceCsv, downloadSourceCsv } from './download-new-csv';
import { deleteNormalizedCsv, normalizeCsv }  from './normalize-csv';
import { resetDb, copyFromCSV, getEvictionsRows } from './import-csv';
import { processEvictionPlaintiffs } from './processEvictionPlaintiffs';
// import { processEvictionPlaintiffsInBulk } from './processEvictionPlaintiffs';

(async () => {
  try {
    console.log("cleaning up existing source csv");
    await deleteSourceCsv();
    console.log("downloading new source csv");
    await downloadSourceCsv();
    console.log("cleaning up existing normalized csv");
    await deleteNormalizedCsv();
    console.log("normalize csv");
    await normalizeCsv();
    console.log("starting to reset");
    await resetDb(db);
    console.log("going to do a copy")
    await copyFromCSV(db);
    console.log("counting rows");
    const count = await getEvictionsRows(db);
    console.log("rows count: ", count);
    // process the eviction rows into plaintiff and plaintiffGroup
    await processEvictionPlaintiffs(db);
    // await processEvictionPlaintiffsInBulk(db);
  } catch (error) {
    console.log(error);
  }
})();