
import {
  sql
} from 'kysely';

import { DatabaseInstance } from "../database";


export async function resetDb(db: DatabaseInstance) {
  const results = await sql`TRUNCATE TABLE "eviction", "plaintiff", "plaintiffGroup" CASCADE;`.execute(db);
  
  console.log("results", results);
}

export async function copyFromCSV(db: DatabaseInstance) {

  const results = await sql`COPY "eviction"(
    "county", 
    "court", 
    "presidingJudge", 
    "docketNumber", 
    "dateFiled", 
    "caseStatus", 
    "plaintiff", 
    "plaintiffZip", 
    "defendant", 
    "defendantZip", 
    "hearingDate", 
    "hearingTime", 
    "claimAmount", 
    "judgementAmount", 
    "rentInArrears", 
    "filingFees", 
    "costs", 
    "serverFees", 
    "damages", 
    "attorneyFees", 
    "rentReservedAndDue", 
    "interest", 
    "monthlyRent", 
    "withdrawn", 
    "dismissed", 
    "grantPossession", 
    "grantPossessionIfJudgeNotSatisfied", 
    "orderForPossessionRequired", 
    "orderForPossessionServed", 
    "judgementForPlaintiff", 
    "judgementForDefendant", 
    "settled", 
    "stayed", 
    "appealed", 
    "plaintiffAttorney", 
    "defendantAttorney", 
    "notes"
    ) 
    FROM '/tmp/LT_All.csv' 
    DELIMITER '\t' 
    CSV HEADER encoding 'UTF8';`.execute(db);
  console.log('csv results', results);
}

export async function getEvictionsRows(db: DatabaseInstance) {
  const { count } = db.fn;
  return db.selectFrom('eviction').select(count<number>('id').as('rowCount')).execute();
}

