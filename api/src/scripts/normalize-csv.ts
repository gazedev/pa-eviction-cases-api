import { createReadStream, createWriteStream, rmSync } from "fs";
import { parse, stringify } from 'csv';

export async function deleteNormalizedCsv() {
  rmSync('/tmp/LT_All.csv', {
    force: true,
  });
}

export async function normalizeCsv() {
  return new Promise((resolve, _reject) => {
    let input  = '/tmp/LT_All_source.csv';
    let output = '/tmp/LT_All.csv';

    let readStream  = createReadStream(input);
    let writeStream = createWriteStream(output)
      .on('finish', () => {
        console.log('write stream finished');
        resolve(true);
      });
    
    let parser = parse({
      delimiter: '\t', 
      quote: '', 
      escape: '"', 
      skip_empty_lines: true
    });

    let stringifier = stringify({
      delimiter: "\t",
    });

    readStream
      .pipe(parser)
      .pipe(stringifier)
      .pipe(writeStream);
      
  });
  
}


