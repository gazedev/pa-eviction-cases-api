
import { get } from 'https';
import { rmSync, createWriteStream } from "fs";

export async function deleteSourceCsv() {
  rmSync('/tmp/LT_All_source.csv', {
    force: true,
  });
}

export function downloadSourceCsv() {
  const file = createWriteStream("/tmp/LT_All_source.csv");
  return new Promise<void>((resolve, _reject) => {
    get("https://raw.githubusercontent.com/pinkushn/pa-evictions/master/webdata/csvs/LT_All.csv", function(response) {
      response.pipe(file);
    
      // after download completed close filestream
      file.on("finish", () => {
          file.close();
          console.log("Download Completed");
          resolve();
      });
    });
  });
}

