// we want to copy eviction plaintiff to plaintiff.original, process it, and then copy it to plaintiff.processed

import { DatabaseInstance } from "../database";
import { db } from "../database";

export async function processEvictionPlaintiffs(db: DatabaseInstance) {

  const query = db.selectFrom('eviction')
    .select(['id', 'plaintiff', 'Plaintiff'])
    .where('Plaintiff', 'is', null);

  const evictionRows = await query.execute();

  let existingPlaintiffs = 0;
  let newPlaintiffs = 0;

  const total = evictionRows.length;
  console.log("Processing", total, "eviction plaintiffs");
  let count = 0;

  for (const row of evictionRows) {
    count++;
    if (count % 1000 === 0) {
      console.log("Processed", count, "of", total);
    }
    const {id, plaintiff} = row;
    if (plaintiff === null) {
      continue;
    }
    // update Plaintiff to be the id of the Plaintiff.original that matches plaintiff
    const Plaintiff = await db.selectFrom('plaintiff')
      .select(['id', 'original'])
      .where('original', '=', plaintiff)
      .executeTakeFirst();

    if (Plaintiff?.id) {
      existingPlaintiffs++;
      // console.log("Plaintiff exists, assigning to eviction");
      await db.updateTable('eviction')
        .set({'Plaintiff': Plaintiff.id})
        .where('id', '=', id)
        .execute();
      continue;
    }
    newPlaintiffs++;
    // console.log("Plaintiff does not exist, creating");
    // create a new Plaintiff.original
    const processedPlaintiff = processOriginalPlaintiff(plaintiff);

    const newPlaintiff = await db.insertInto('plaintiff')
      .values({
        original: plaintiff,
        processed: processedPlaintiff,
      })
      .returning(['id'])
      .executeTakeFirstOrThrow();

    await db.updateTable('eviction')
      .set({Plaintiff: newPlaintiff.id})
      .where('id', '=', id)
      .executeTakeFirstOrThrow();

  }
  console.log("Processing complete", {existingPlaintiffs, newPlaintiffs});
}

export async function processEvictionPlaintiffsInBulk(db: DatabaseInstance) {
  let existingPlaintiffs = 0;
  let newPlaintiffs = 0;

  let keepGoing = true;
  while (keepGoing) {
    const row = await getNextEvictionRow(db);
    if (row.length === 0) {
      keepGoing = false;
      continue;
    }

    const {plaintiff} = row[0];

    if (plaintiff === null) {
      continue;
    }

    // update Plaintiff to be the id of the Plaintiff.original that matches plaintiff
    const Plaintiff = await db.selectFrom('plaintiff')
      .select(['id', 'original'])
      .where('original', '=', plaintiff)
      .executeTakeFirst();

    if (Plaintiff?.id) {
      // This shouldn't happen, because we should have already processed this plaintiff
      existingPlaintiffs++;
      // console.log("Plaintiff exists, assigning to eviction");
      await db.updateTable('eviction')
        .set({'Plaintiff': Plaintiff.id})
        .where('plaintiff', '=', plaintiff)
        .execute();
      continue;
    }
    newPlaintiffs++;

    const processedPlaintiff = processOriginalPlaintiff(plaintiff);

    const newPlaintiff = await db.insertInto('plaintiff')
      .values({
        original: plaintiff,
        processed: processedPlaintiff,
      })
      .returning(['id'])
      .executeTakeFirstOrThrow();

    await db.updateTable('eviction')
      .set({Plaintiff: newPlaintiff.id})
      .where('plaintiff', '=', plaintiff)
      .executeTakeFirstOrThrow();

  }

  console.log("Processing complete", {existingPlaintiffs, newPlaintiffs});
}

function getNextEvictionRow(db: DatabaseInstance) {
  const query = db.selectFrom('eviction')
  .select(['id', 'plaintiff', 'Plaintiff'])
  .where('Plaintiff', 'is', null)
  .limit(1);

  return query.execute();
}

function processOriginalPlaintiff(plaintiff: string) {
  const split = plaintiff.split(' MDJS 1200 ');
  // it either returns the part before the split, or the original string
  return split[0];
}

(async () => {
  console.log("starting to process eviction plaintiffs")
  try {
    await processEvictionPlaintiffs(db);
  } catch (error) {
    console.log(error);
  }
})();