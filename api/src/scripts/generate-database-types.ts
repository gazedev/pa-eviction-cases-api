import { modules } from '../modules/enabled';
import { writeFileSync } from 'fs';

(async () => {

  const importModules = [];

  const importContent = [];
  const interfaceContent = [];

  for (const module of modules) {
    const importPath = constructImportPath(module);
    importModules.push({
      module,
      imports: import(importPath)
    });
  }
  interfaceContent.push(`export interface Database {`);


  await Promise.all(importModules.map(item => item.imports));

  for (const item of importModules) {
    const imported = await item.imports;
    const importInterfaces = [];
    for (const [key, value] of Object.entries(imported.interfaces)) {
      importInterfaces.push(value);
      interfaceContent.push(`  ${key}: ${value},`);
    }
    importContent.push(`import { ${importInterfaces.join(", ")} } from "${constructImportPathToWrite(item.module)}";`)

  }
  interfaceContent.push('};');
  const toFile = [...importContent, '', ...interfaceContent, ''].join('\n');
  console.log(toFile);
  writeFileSync('./src/databaseTypes.generated.ts', toFile);

})();

function constructImportPath(moduleName: string) {
  return `../modules/${moduleName}/entities`
}

function constructImportPathToWrite(moduleName: string) {
  return `./modules/${moduleName}/entities`
}
