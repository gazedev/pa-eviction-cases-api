'use strict';

import Hapi from "@hapi/hapi";
import { Server } from "@hapi/hapi";

export const init = async function(): Promise<Server> {
    const server = Hapi.server({
        port: process.env.PORT || 8081,
        host: '0.0.0.0'
    });

    // Routes will go here
    server.route({
      path: "/",
      method: "GET",
      handler: (_request, h) => {
        return h.response({
          status: "up",
          documentation: "/documentation",
          license: "/license",
        });
      },
    });

    return server;
};

export const start = async function (server: Server): Promise<void> {
    console.log(`Server Listening on :${server.settings.port}`);
    await server.start();
};

// istanbul ignore next
process.on('unhandledRejection', (err) => {
    console.error("unhandledRejection");
    console.error(err);
    process.exit(1);
});
