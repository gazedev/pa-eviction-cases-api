import * as Hapi from '@hapi/hapi';
import * as Inert from '@hapi/inert';
import * as Vision from '@hapi/vision';
import * as HapiSwagger from 'hapi-swagger';

const swaggerOptions = {
  info: {
    title: 'Evictions API Documentation',
    version: '1.0',
  },
};

const plugins: Array<Hapi.ServerRegisterPluginObject<any>> = [
  {
    plugin: Inert
  },
  {
    plugin: Vision
  },
  {
    plugin: HapiSwagger,
    options: swaggerOptions
  }
];

export const documentRoutes = async (server: Hapi.Server) => {
  await server.register(plugins);
}

