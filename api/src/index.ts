/* istanbul ignore file - don't need to test basic start calls */

import { init, start } from "./server";
import { loadRoutes } from './routes';
import {documentRoutes } from './document-routes';

init().then(async (server) => {

  loadRoutes(server);

  await documentRoutes(server)

  return start(server)

});
