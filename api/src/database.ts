import { Pool } from 'pg';
import {
  Kysely,
  PostgresDialect
} from 'kysely';
import { config } from './config';
import { Database } from './databaseTypes.generated';

export type DatabaseInstance = Kysely<Database>;

export const db: DatabaseInstance = new Kysely<Database>({
  dialect: new PostgresDialect({
    pool: new Pool({
      connectionString: config.DATABASE_URL,
    })
  })
})