import * as serverFile from "./server";

it("has a root route", async () => {
  const mockReponse = jest.fn();
  const mockH = {
    response: mockReponse,
  };
  const server = await serverFile.init();
  const index = server.table().findIndex(item => {
    return (item.method === "get") && (item.path === "/");
  });
  expect(index).toBeGreaterThanOrEqual(0);
  let routeSettings = server.table()[index].settings;
  if (typeof routeSettings?.handler === "function") {
    routeSettings.handler(undefined as any, mockH as any);
  }
  expect(mockReponse).toBeCalledWith(expect.objectContaining(
    {
      status: expect.any(String),
      documentation: expect.any(String),
      license: expect.any(String),
    }
  ));
});

it("server start starts the server", async () => {
  const mockStart = jest.fn();
  const mockServer = {
    start: mockStart,
    settings: {
      port: "1234"
    }
  };
  await serverFile.start(mockServer as any);
  expect(mockStart).toBeCalled();
});
