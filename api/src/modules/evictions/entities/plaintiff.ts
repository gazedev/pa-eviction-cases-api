// create a kysely table interface for "plaintiff" that has a name columnm and a "plaintiffGroup" column that is a foreign key to the plaintiffGroup table

import {
  Generated,
} from 'kysely';

export type PlaintiffTable = {
  id: Generated<string>,
  original: string,
  processed?: string,
  PlaintiffGroup?: string,
}
