// export type PlaintiffGroupTable with a name, and external key

import {
  Generated,
} from 'kysely';

export type PlaintiffGroupTable = {
  id: Generated<string>,
  name: string,
}