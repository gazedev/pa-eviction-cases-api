export { EvictionTable } from "./eviction";
export { PlaintiffTable } from "./plaintiff";
export { PlaintiffGroupTable } from "./plaintiffGroup";

// We can't write types' names to file on their own
// because they aren't there at run time.
// So, we have to export them as a run time value.
export const interfaces = {
  eviction: "EvictionTable",
  plaintiff: "PlaintiffTable",
  plaintiffGroup: "PlaintiffGroupTable",
}