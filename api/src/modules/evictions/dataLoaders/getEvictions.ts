import { db } from '../../../database';

type EvictionsFilter = {
  county?: string,
  plaintiff?: string[]
};
export const getEvictions = async (filter: EvictionsFilter) => {
  const { count } = db.fn;

  let result;
  let total;
  
  try {
    let qb = db
    .selectFrom('eviction')

    if (filter.county) {
      qb = qb.where('county', '=', filter.county)
    }

    if (filter.plaintiff?.length) {
      const plaintiffs = filter.plaintiff;

      qb = qb.where((subQb) => {
        plaintiffs.forEach(plaintiff => {
          subQb = subQb.orWhere('plaintiff', 'ilike', plaintiff)
        });
        return subQb;
      });
    }

    const totalQuery = await qb
      .select(count('id').as('total'))
      .execute();

    total = totalQuery[0].total;

    result = await qb
      .select(['id', 'county', 'plaintiff', 'caseStatus', 'dateFiled', 'presidingJudge', 'claimAmount'])
      .limit(1000)
      .execute();

  } catch (error) {
    console.log(error)
  }
  return {
    pageInfo: {
      total
    },
    result
  };
}