import { getEvictionsHandler } from "./handlers/getEvictions"


// TODO: we probably need to do some sort of processed name
// if we just feed every variation from pittsburgh housing to this api,
// we might get something like "meyers" for meyers management, 
// but that would wrongly match "Community Builders dba Meyers Ridge Townhomes, Phase One"
export const routes = [
  getEvictionsHandler
];
