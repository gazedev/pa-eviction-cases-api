import { ServerRoute } from "@hapi/hapi";
import { getEvictions } from "../dataLoaders/getEvictions";
import Joi from "joi";

export const getEvictionsHandler: ServerRoute = {
  method: 'GET',
  path: '/evictions',
  options: {
    description: 'Get Eviction court records',
    notes: 'Returns all eviction court records.',
    tags: ['api', 'Eviction'],
    validate: {
      query: Joi.object().keys({
        county: Joi.string().optional(),
        plaintiff: Joi.array().items(Joi.string()).single().optional(),
      }),
    }
  },
  handler: async (req) => {
    try {
      return await getEvictions(req.query);
    } catch (error) {
      console.log(error);
      return;
    }
  }
};





