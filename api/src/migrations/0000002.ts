import { Kysely, sql } from 'kysely'

export async function up(db: Kysely<any>): Promise<void> {

  await db.schema
    .createTable('plaintiffGroup')
    .addColumn('id', 'uuid', (col) =>
      col.primaryKey().defaultTo(sql`gen_random_uuid()`)
    )
    .addColumn('name', 'varchar')
    .execute();

  await db.schema
    .createTable('plaintiff')
    .addColumn('id', 'uuid', (col) =>
      col.primaryKey().defaultTo(sql`gen_random_uuid()`)
    )
    .addColumn('original', 'varchar')
    .addColumn('processed', 'varchar')
    .addColumn('PlaintiffGroup', 'uuid', (col) => {
      return col
        .references('plaintiffGroup.id')
        .onUpdate('cascade')
        .onDelete('cascade');
    })
    .execute();

  await db.schema
    .alterTable('eviction')
    .addColumn('Plaintiff', 'uuid', (col) => {
      return col
        .references('plaintiff.id')
        .onUpdate('cascade')
        .onDelete('cascade');
    })
    .execute();

}

export async function down(): Promise<void> {}
