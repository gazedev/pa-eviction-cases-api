import { Kysely, sql } from 'kysely'

export async function up(db: Kysely<any>): Promise<void> {

  await sql`CREATE EXTENSION IF NOT EXISTS pgcrypto;`.execute(db);
  await db.schema
    .createTable('eviction')
    .addColumn('id', 'uuid', (col) =>
      col.primaryKey().defaultTo(sql`gen_random_uuid()`)
    )
    .addColumn('county', 'varchar')
    .addColumn('court', 'varchar')
    .addColumn('presidingJudge', 'varchar')
    .addColumn('docketNumber', 'varchar')
    .addColumn('dateFiled', 'varchar')
    .addColumn('caseStatus', 'varchar')
    .addColumn('plaintiff', 'varchar')
    .addColumn('plaintiffZip', 'varchar')
    .addColumn('defendant', 'varchar')
    .addColumn('defendantZip', 'varchar')
    .addColumn('hearingDate', 'varchar')
    .addColumn('hearingTime', 'varchar')
    .addColumn('claimAmount', 'varchar')
    .addColumn('judgementAmount', 'varchar')
    .addColumn('rentInArrears', 'varchar')
    .addColumn('filingFees', 'varchar')
    .addColumn('costs', 'varchar')
    .addColumn('serverFees', 'varchar')
    .addColumn('damages', 'varchar')
    .addColumn('attorneyFees', 'varchar')
    .addColumn('rentReservedAndDue', 'varchar')
    .addColumn('interest', 'varchar')
    .addColumn('monthlyRent', 'varchar')
    .addColumn('withdrawn', 'varchar')
    .addColumn('dismissed', 'varchar')
    .addColumn('grantPossession', 'varchar')
    .addColumn('grantPossessionIfJudgeNotSatisfied', 'varchar')
    .addColumn('orderForPossessionRequired', 'varchar')
    .addColumn('orderForPossessionServed', 'varchar')
    .addColumn('judgementForPlaintiff', 'varchar')
    .addColumn('judgementForDefendant', 'varchar')
    .addColumn('settled', 'varchar')
    .addColumn('stayed', 'varchar')
    .addColumn('appealed', 'varchar')
    .addColumn('plaintiffAttorney', 'varchar')
    .addColumn('defendantAttorney', 'varchar')
    .addColumn('notes', 'varchar')
    .execute()
}

export async function down(db: Kysely<any>): Promise<void> {
  await db.schema.dropTable('eviction').execute();
}
