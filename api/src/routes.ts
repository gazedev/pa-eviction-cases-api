import * as Hapi from '@hapi/hapi';

import { modules } from "./modules/enabled";

export const loadRoutes = async (server: Hapi.Server) => {
  // Build the routes of all our modules, injecting the models into each
  for (let mod of modules) {
    let routesFile;
    try {
      routesFile = await import(`./modules/${mod}/routes`);
      if(routesFile.routes) {
        await server.route(routesFile.routes);
      }
    } catch(err) {
      console.log(err);
      console.log(`module ${mod} did not have a routes file or hapi failed to register them`);
    }
  }

}

