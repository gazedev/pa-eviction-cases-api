#!/bin/bash
# This script makes assumptions about where you are in the filesystem
# so this makes sure it executes from the context it thinks it is
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd $DIR

mkdir -p tmp postgres-data api/node_modules /home/node/.npm

git pull

docker-compose --file=docker-compose.yml stop

docker-compose --file=docker-compose.yml up -d
